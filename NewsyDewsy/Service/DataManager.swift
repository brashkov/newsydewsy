//
//  DataManager.swift
//  NewsyDewsy
//
//  Created by Boris Rashkov on 14.05.18.
//  Copyright © 2018 BRashkovOOD. All rights reserved.
//

import Foundation

class DataManager {
    
    private var newsArr:[NewsArticle] = []
    
    static let instance = DataManager()
    
    private init() {
    }
    
    
    public func addArticle(_ article:NewsArticle) -> NewsArticle{
        for news in newsArr {
            if news.equalsTo(article){
                print ("Article is already added")
                return article
            }
        }
        newsArr.append(article)
        return article
    }
    
    public func removeArticle(with index:Int){
        if newsArr.count > index {
            newsArr.remove(at: index)
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .dataUpdated, object: nil)
            }
        }else{
            print ("There is no such element")
        }
    }
    
    public func getArticle(By index:Int) -> NewsArticle? {
        //TO-DO: Return a copy of it
        if newsArr.count > index {
            return newsArr[index]
        }else{
            return nil
        }
    }
    
    public func count() -> Int{
        return newsArr.count
    }
}

