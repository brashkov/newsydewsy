//
//  FetchingService.swift
//  NewsyDewsy
//
//  Created by Boris Rashkov on 31.05.18.
//  Copyright © 2018 BRashkovOOD. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class FetchingService{
    
    private let API_KEY = "38867f0c97e64e26b0666edd5c043ed3"
    private let TOP_HEADLINES = "https://newsapi.org/v2/top-headlines?"
    private let EVERY_HEADLINE = "https://newsapi.org/v2/everything?"
    
    private let testReq = "https://newsapi.org/v2/everything?q=technology+swift+programming&sortBy=popularity&language=en&page=1&apiKey=38867f0c97e64e26b0666edd5c043ed3"
    
    public func getTopHeadlines(){
        
       // let request = String(format: "%@?location=%@&radius=%@&type=%@&key=%@", arguments: [NEARBY_URL,location,radius,type,API_KEY])
       // print (request)
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
            
        }
    }
    
    public func getEverything(completion: @escaping (JSON?)->() ){
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
            
            Alamofire.request(self.testReq)
                .validate()
                .responseJSON(completionHandler: { (response) in
                    
                    guard response.result.error == nil else{
                        completion(nil)
                        return
                    }
                    
                    guard let value = response.result.value else{
                        print ("cant retrieve json")
                        completion(nil)
                        return
                    }
                    
                    let jsonData = JSON(value)
                    //completion(jsonData)
                    
                    let totalResults = jsonData["totalResults"].stringValue
                    
                    
                    
                    //Adding articles to shared memory
                    for article in jsonData["articles"].arrayValue {
                        let tmp = NewsArticle(with: article)
                        if(tmp.imageURL != ""){
                            DataManager.instance.addArticle(tmp)
                        }
                    }
                    
                    completion(jsonData)
                })
        }
    }
}
