//
//  Extensions.swift
//  NewsyDewsy
//
//  Created by Boris Rashkov on 31.05.18.
//  Copyright © 2018 BRashkovOOD. All rights reserved.
//

import Foundation

extension Notification.Name {
    
    static let dataUpdated = Notification.Name(rawValue: "dataUpdated")

}
