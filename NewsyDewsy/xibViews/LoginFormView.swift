//
//  LoginFormView.swift
//  NewsyDewsy
//
//  Created by Boris Rashkov on 10.05.18.
//  Copyright © 2018 BRashkovOOD. All rights reserved.
//

import UIKit

class LoginFormView: UIView {

    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var smthShare: UITextView!
    
    var textViews:[UIView]!
    
    override func awakeFromNib() {
        textViews.append(firstName)
        textViews.append(lastName)
        textViews.append(username)
        textViews.append(password)
        textViews.append(smthShare)
        
        layerSetup()
    }
    
    private func layerSetup(){
        
    }
}
