//
//  ArticleCell.swift
//  NewsyDewsy
//
//  Created by Boris Rashkov on 1.05.18.
//  Copyright © 2018 BRashkovOOD. All rights reserved.
//

import UIKit
import SkeletonView
import Alamofire

class ArticleCell: UICollectionViewCell {
    
    // make the default to be the image of the site or newspaper of there is no image provided for the specific feed

    @IBOutlet weak var articleImage: UIImageView!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var titleView: UITextView!
    
    //constrains
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    
    
    
    public func cellSetup(){
        
        imageFrame()
        
        titleView.backgroundColor = UIColor.clear
        
        articleImage.showAnimatedGradientSkeleton()
        //authorLabel.showAnimatedGradientSkeleton()
        titleView.showAnimatedGradientSkeleton()
    
    }
    
    private func imageFrame(){
        imageHeight.constant = self.frame.height * 2/3
        
        
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.black.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
        
        self.layoutIfNeeded()
        
    }
    
    public func populateData(with article:NewsArticle, for indexPath:IndexPath){

        titleView.text = article.title
        titleView.textColor = UIColor.black
        titleView.hideSkeleton()
        
        authorLabel.text = article.author
        authorLabel.textColor = UIColor.white
        
        
        guard article.downloadedImage == nil else {
            articleImage.image = article.downloadedImage!
            articleImage.hideSkeleton()
            return
        }
            if let url = article.imageURL{
                let imgURL = URL(string: url)
                DispatchQueue.global().async {
                    
                    do {
                        let imageData:NSData = try NSData(contentsOf: imgURL!)
                        DispatchQueue.main.async {
                            if let image = UIImage(data: imageData as Data){
                                self.articleImage.image = image
                                article.downloadedImage = image
                                self.articleImage.hideSkeleton()
                            }
                        }
                    }catch {
                        print ("Error !!!!")
                        DataManager.instance.removeArticle(with: indexPath.row)
                    }
                }
            }
    }
    
   private func ResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio) //width: size.width * heightRatio, heigh: size.height * heightRatio
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio) //size.width * widthRatio,  size.height * widthRatio
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height) //0, 0, newSize.width, newSize.height
        
            // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    
        return newImage!
    }
}
