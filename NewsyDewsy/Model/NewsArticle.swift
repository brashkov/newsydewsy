//
//  NewsArticle.swift
//  NewsyDewsy
//
//  Created by Boris Rashkov on 1.05.18.
//  Copyright © 2018 BRashkovOOD. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit

class NewsArticle {

    let title:String!
    let description:String!
    let imageURL:String?
    
    let articleURL:String!
    let sourceID:String?
    let sourceName:String?
    //test new line
    let author:String?
    
    
    //ading a test var
    let fu = 5
    var downloadedImage:UIImage?
    
    init(with json:JSON){
        
        title = json["title"].stringValue
        description = json["description"].stringValue
        imageURL = json["urlToImage"].stringValue
        articleURL = json["url"].stringValue
        sourceID = json["source"]["id"].stringValue
        sourceName = json["source"]["name"].stringValue
        author = json["author"].stringValue
        
        downloadedImage = nil
        
    }
    
    public func equalsTo(_ article:NewsArticle) -> Bool {
        return self.title.lowercased() == article.title.lowercased() ? true : false
    }

}
