//
//  SplashScreenController.swift
//  NewsyDewsy
//
//  Created by Boris Rashkov on 10.05.18.
//  Copyright © 2018 BRashkovOOD. All rights reserved.
//

import UIKit

class SplashScreenController: UIViewController {

    @IBOutlet weak var appLogo: UIImageView!
    
    @IBOutlet weak var footer: UILabel!
    @IBOutlet var buttons: [UIButton]!
    
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var guestButton: UIButton!
    
    private var credView:UIView?
    
    var gradientLayer: CAGradientLayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        let pinkBG = UIColor(red: 255/255, green: 97 / 255, blue: 93 / 255, alpha: 1.0)
        self.view.backgroundColor = pinkBG
        appLogo.image = #imageLiteral(resourceName: "appLogo3").withRenderingMode(.alwaysTemplate)
        appLogo.tintColor = .white
        
        footer.alpha = 0
        footer.textColor = .white
        footer.layer.cornerRadius = 0.47 * footer.frame.height
        
    
        
        for button in buttons {
            button.alpha = 0
            button.setTitleColor(.white, for: .normal)
            button.layer.cornerRadius = 0.47 * button.frame.height
            button.layer.borderColor = UIColor.white.cgColor
            button.layer.borderWidth = 3
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        sleep(2)
        UIView.animate(withDuration: 1) {
            self.appLogo.center.y = self.appLogo.frame.height / 2 + (self.view.bounds.height * (1/7))
            self.footer.alpha = 1
        }
        UIView.animate(withDuration: 1) {
            for button in self.buttons {
                button.alpha = 1
                //button.center.y = button.center.y
            }
        }
    }
    
    private func credentialsField(){
        let heigh = guestButton.frame.minY - appLogo.frame.maxY - 2 * 15 // 15 must be the margin top and bottom
        let width = guestButton.frame.width
        let xStart = guestButton.frame.minX
        let yStart = guestButton.frame.minY - (heigh + 15)
        
        print ("\(guestButton.frame.minY) -- \(guestButton.frame.maxY)")
        let frame = CGRect(x: xStart, y: yStart, width: width, height: heigh)
        
        credView = UIView(frame: frame)
        credView!.layer.cornerRadius = 0.20 * credView!.frame.height
        credView!.layer.borderWidth = 5
        credView!.layer.borderColor = UIColor.white.cgColor
    }
    
    //MARK: Buttons functions
    @IBAction func signUp(_ sender: Any) {
        self.credentialsField()
        UIView.animate(withDuration: 1) {
            self.appLogo.center = self.view.center
            self.appLogo.alpha = 0
            self.signUpButton.center = self.guestButton.center
            self.loginButton.center = self.guestButton.center
            self.loginButton.alpha = 0
            self.guestButton.alpha = 0
            //self.view.addSubview(self.credView!)
        }
    }
    
    @IBAction func logIn(_ sender: Any) {
        UIView.animate(withDuration: 1) {
            self.loginButton.center = self.guestButton.center
            self.signUpButton.center = self.guestButton.center
            self.signUpButton.alpha = 0
            self.guestButton.alpha = 0
        }
    }
    
    @IBAction func guestUser(_ sender: Any) {
    }
    
}
