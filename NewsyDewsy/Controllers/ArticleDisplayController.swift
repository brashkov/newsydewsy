//
//  ArticleDisplayController.swift
//  NewsyDewsy
//
//  Created by Boris Rashkov on 1.05.18.
//  Copyright © 2018 BRashkovOOD. All rights reserved.
//

import UIKit
import SkeletonView

class ArticleDisplayController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    private let defaultIdentifier = "ArticleCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(UINib(nibName: "ArticleCell", bundle: nil),forCellWithReuseIdentifier: defaultIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.backgroundColor = .white
        
        let test = FetchingService()
        test.getEverything { (response) in
            print ( response != nil )
            self.collectionView.reloadData()
        }
        
        self.addObservers()
    }
    
    private func addObservers(){
         NotificationCenter.default.addObserver(self, selector: #selector(refreshData), name: .dataUpdated, object: nil)
    }
    
    @objc private func refreshData(){
            self.collectionView.reloadData()
            
    }

}
//MARK: Layout
extension ArticleDisplayController: UICollectionViewDelegateFlowLayout {
    
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

            let Cwidht:CGFloat = self.view.frame.width
            let Cheight:CGFloat = self.view.frame.height / 3
            
            return CGSize(width: Cwidht, height: Cheight)
    }
    
    
}
//MARK: Datasource
extension ArticleDisplayController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataManager.instance.count() <= 0 ? 10 : DataManager.instance.count()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: defaultIdentifier, for: indexPath)
        
        if let article = cell as? ArticleCell {
            article.cellSetup()
            
            if let articleNews = DataManager.instance.getArticle(By: indexPath.row){
               article.populateData(with: articleNews, for: indexPath)
                
            }
        }
        
        
        // textView part isnt properly masked to the superview
        
        return cell
    }
}
