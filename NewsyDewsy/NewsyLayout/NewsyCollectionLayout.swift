//
//  NewsyCollectionLayout.swift
//  NewsyDewsy
//
//  Created by Boris Rashkov on 1.05.18.
//  Copyright © 2018 BRashkovOOD. All rights reserved.
//

import UIKit

class NewsyCollectionLayoutAttributes : UICollectionViewLayoutAttributes{
    
}

class NewsyCollectionLayout: UICollectionViewLayout {

    let itemSize = CGSize(width: 350, height: 210)
    
    let itemSpacing:CGFloat = 0
    let headerSize:CGFloat = 25
    let footerSize:CGFloat = 15
    
    
    var attributesList = [NewsyCollectionLayoutAttributes]()
    
    override var collectionViewContentSize: CGSize {
       return CGSize(width: collectionView!.bounds.width,
                     height: CGFloat((collectionView!.numberOfItems(inSection: 0)))*(itemSize.height + itemSpacing) + headerSize + footerSize)
    }
    
    
    override class var layoutAttributesClass: AnyClass {
        return NewsyCollectionLayoutAttributes.self
    }
    
    /*
     * --- This method is called when the layout is invalidated (when the whole layout is being recalculated)
     
     In short, you iterate over each item in the collection view and execute the closure. Keep reading for a line-by-line explanation:
     
     1.Create an instance of CircularCollectionViewLayoutAttributes for each index path, and then set its size.
     
     2.Position each item at the center of the screen.
     
     3......
     */
    
    override func prepare() {
        super.prepare()
        
        attributesList = (0...collectionView!.numberOfItems(inSection: 0)-1).map { (i)
            -> NewsyCollectionLayoutAttributes in
            
            let centerY = headerSize + (CGFloat(i) * (itemSize.height + itemSpacing)) + itemSize.height/2
            
            let attributes = NewsyCollectionLayoutAttributes(forCellWith: IndexPath(item: i, section: 0))
            
            attributes.size = CGSize(width: collectionView!.bounds.width, height: itemSize.height)
            
             attributes.center = CGPoint(x: self.collectionView!.bounds.midX, y: centerY)
            
            return attributes
        }
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return attributesList
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return attributesList[indexPath.row]
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }

}
